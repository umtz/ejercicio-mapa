<?php

$dao_hostname = "localhost";
$dao_user = "root";
$dao_password = "root";
$dao_db_name = "honda";
$dao_db_port = "8889";

function get_connection()
{
    global $dao_hostname, $dao_user, $dao_password, $dao_db_name, $dao_db_port;

    $dao_connection = mysqli_connect($dao_hostname, $dao_user, $dao_password, $dao_db_name, $dao_db_port);
    if (!$dao_connection) {
        die("Error de conexión a la base de datos");
    }
    return $dao_connection;
}

function db_get_all($table, $select = "*", $with = array())
{
    $connection = get_connection();
    $query = "select $select from $table";
    foreach ($with as $entity){
        $otherTable = $entity."s";
        $query .= " JOIN $otherTable ON $otherTable.id = $table.$entity"."_id";
    }
    $results = mysqli_query($connection, $query);
    if (!$results) {
        die('Error DAO select states');
    }
    mysqli_close($connection);
    return $results;
}

function db_delete_from_table($table, $id)
{
    $connection = get_connection();
    $result = mysqli_query($connection, "delete from $table where id=" . $id);
    mysqli_close($connection);
    return $result;
}

function db_insert_to_table($table, $data)
{
    $connection = get_connection();
    $keys = "";
    $values = "";
    foreach ($data as $key => $value) {
        $keys .= "$key, ";
        $values .= "'$value', ";
    }
    $keys = rtrim($keys, ', ');
    $values = rtrim($values, ', ');
    return mysqli_query($connection, "insert into $table ($keys) values ($values);");
}

function validate_credentials($username, $password){
    $connection = get_connection();
    return mysqli_query($connection, "select * from users where username = '$username' and password = '$password';");
}