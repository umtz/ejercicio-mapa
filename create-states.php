<?php
    session_start();
    if ($_SESSION['is_logged_in'] == false) {
        header('Location: login.php');
    }
?><!DOCTYPE html>
<html>
	<head>
		<title>Crear estados</title>
		<meta charset="utf-8">
		<script src="js/hola.js" type="text/javascript"></script>
		<script   src="https://code.jquery.com/jquery-3.1.1.min.js"   integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="   crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	</head>
	<body>
		<div class="container">
		        <h1> Admin Zone </h1>
		        <h2>Crear Estados</h2>
		        <nav>
		          <a href="admin.php" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-left"></i>Volver</a>
		        </nav>
		
		        <form method="post" action="save-state.php" class="form-horizontal" method="post">
		        	<div class="form-group">
		        		<label class="control-label col-sm-2">Estado</label>
		        		<div class="col-sm-8">
		        			<input type="text" name="name" value="" class="form-control"/>
		        		</div>
		        		<div class="col-sm-2">
		        			<button class="btn btn-info">Agregar</button>
		        		</div>
		        	</div>
		        </form>
		</div>
	</body>
</html>