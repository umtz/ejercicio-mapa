<?php 
    session_start();
    if ($_SESSION['is_logged_in'] == false) {
        header('Location: login.php');
    }

	include "dao.php";
?>
<!DOCTYPE html>
<html>
<head>
    <title>Crear Sucursal</title>
    <link rel="stylesheet" type="text/css" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>
<div class="container">
    <h1>Amin zone</h1>
    <h2>Crear sucursal</h2>
    <nav>
        <a href="admin.php" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-left"></i> Volver</a>
    </nav>
    <form class="form-horizontal" action="save-branch.php" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label class="control-label col-sm-2">Sucursal:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="name">
            </div>
        </div>
        <div class="form-group">
        	<lebel class="col-sm-2 control-label">Estados:</lebel>
        	<div class="col-sm-10">
        		<select class="form-control" name="state_id">
        			<?php 
        				$estados = db_get_all('states');
        				while ($estado = mysqli_fetch_array($estados)) {
        					?>
        						<option value="<?= $estado['id'] ?>"><?= $estado['name'] ?></option>
        					<?php
        				}
        			?>
        		</select>
        	</div>
        </div>
        <div class="form-group">
        	<label class="col-sm-2 control-label">Imagen:</label>
        	<div class="col-sm-10">
        		<input type="file" class="form-control" accept="image/*" name="image" />
        	</div>
        </div>
        <div class="form-group">
        	<div class="col-sm-12 text-right">
        	    <button class="btn btn-primary">Agregar</button>
        	</div>
        </div>
    </form>
</div>
</body>
</html>