<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <style>
    	body{
    		background-color: gainsboro ;
    	}
    	
    	.form-container{
    		background: white;
    		padding: 1em;
    		border-radius: 0.5em;
    	}
    	h1, h2{
    		text-align: center;
    	}
    	h2{
    		padding-bottom: 1em;
    		border-bottom: 1px solid #ccc;
    		margin-bottom: 0.5em;
    	}
    	h1{
    		font-size: 1.5em;
    	}
    	
    </style>
</head>
<body>
	<div class="container">
		<h1>Sitio de motos</h1>
		<h2>LOGIN</h2>
        <?php 
            
            if(isset($_SESSION['messages']) && count($_SESSION['messages']) > 0){
                echo '<ul>';
                    foreach ($_SESSION['messages'] as $message) {
                        ?>
                            <li><?= $message ?></li>
                        <?php
                    }
                echo '</ul>';
                $_SESSION['messages'] = array();
            }
         ?>
		<div class="form-container">
			<form method="post" action="dologin.php">
				<div class="form-group">
					<label class="control-label">Username:</label>
					<div class="">
						<input type="" name="username" value="" class="form-control"/>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label ">Password:</label>
					<div class="">
						<input type="password" name="password" value="" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<input type="submit" value="Login" class="btn btn-primary" />
				</div>
			</form>
		</div>
	</div>
</body>
</html>